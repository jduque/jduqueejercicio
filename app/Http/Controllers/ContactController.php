<?php

namespace App\Http\Controllers;

use App\Models\contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts['contacts'] = contact::paginate(5);
        return view('contacts.index', $contacts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contacts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields=[
            'name' => 'required|string|max:10',
            'surname' => 'required|string|max:10',
            'email' => 'required|email'
        ];
        $message=["required"=>"El :attribute es requerido!"];
        $this->validate($request, $fields, $message);
        //$contactData = request()->all();
        $contactData = request()->except('_token');
        contact::insert($contactData);
        return redirect('contacts')->with('message', 'Se ha creado el contacto!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(contact $contact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contact = contact::findOrFail($id);
        return view('contacts.edit', compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fields=[
            'name' => 'required|string|max:10',
            'surname' => 'required|string|max:10',
            'email' => 'required|email'
        ];
        $message=["required"=>"El :attribute es requerido!"];
        $this->validate($request, $fields, $message);
        $contactData = request()->except(['_token', '_method']);
        contact::where('id', '=', $id)->update($contactData);
        //$contact = contact::findOrFail($id);
        //return view('contacts.edit', compact('contact'));
        return redirect('contacts')->with('message', 'Se ha modificado el contacto!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        contact::destroy($id);
        return redirect('contacts')->with('message', 'Se ha borrado el contacto!');
    }
}
