@extends('layouts.app')

@section('content')
<div class="container">
    @if(Session::has('message'))
        <div class="alert alert-success" role="alert">
            {{Session::get('message')}}
        </div>
    @endif

    <a href="{{url('contacts/create')}}" class="btn btn-success">Crear Contacto</a>
    <br />
    <br />
    <table class="table table-light table-hover">
        <thead class="thead-light">
            <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Email</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($contacts as $contact)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$contact->Name}}</td>
                    <td>{{$contact->Surname}}</td>
                    <td>{{$contact->Email}}</td>
                    <td>
                        <a href="{{url('/contacts/'.$contact->id.'/edit')}}" class="btn btn-warning">Editar</a>
                        <form action="{{url('/contacts/'.$contact->id)}}" method="post" style="display:inline">
                            {{csrf_field()}}
                            {{method_field('DELETE')}}
                            <button type="submit" onclick="return confirm('¿Borrar?')"  class="btn btn-danger">Borrar</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{$contacts->links()}}
</div>
@endsection