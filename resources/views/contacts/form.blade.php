<div class="form-group">
    <label for="name" class="control-label">{{'Nombre'}}</label>
    <input type="text" class="form-control {{$errors->has('name')?'is-invalid':''}}" name="name" id="name" value="{{isset($contact->Name)?$contact->Name:old('name')}}">
    {!! $errors->first('name','<div class=invalid-feedback>:message</div>') !!}
</div>
<div class="form-group">
    <label for="surname" class="control-label">{{'Apellido'}}</label>
    <input type="text" class="form-control {{$errors->has('surname')?'is-invalid':''}}" name="surname" id="surname" value="{{isset($contact->Surname)?$contact->Surname:old('surname')}}">
    {!! $errors->first('surname','<div class=invalid-feedback>:message</div>') !!}
</div>
<div class="form-group">
    <label for="email" class="control-label">{{'Email'}}</label>
    <input type="text" class="form-control {{$errors->has('email')?'is-invalid':''}}" name="email" value="{{isset($contact->Email)?$contact->Email:old('email')}}">
    {!! $errors->first('email','<div class=invalid-feedback>:message</div>') !!}
</div>
<input type="submit" class="btn btn-success" value="{{$mode=='create' ? 'Crear' : 'Modificar'}}">
<a href="{{url('contacts')}}" class="btn btn-primary">Volver</a>