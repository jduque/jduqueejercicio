@extends('layouts.app')

@section('content')
<div class="container">
    @if(count($errors)>0)
        <div class="alert alert-danger" role="alert">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{url('/contacts/'.$contact->id)}}" method="post" class="form-horizontal">
        {{csrf_field()}}
        {{method_field('PATCH')}}
        @include('contacts.form', ['mode'=>'edit'])
    </form>
</div>
@endsection